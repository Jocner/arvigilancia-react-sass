import React from "react";
import Navbar from "../Navbar";

function NotFound() {
  return (
    <body>
      <Navbar />
      <footer className="footer">
        <p className="footer-by">
          A project by{" "}
         Jocner Patiño Buznego
        </p>
      </footer>
    </body>
  );
}

export default NotFound;
