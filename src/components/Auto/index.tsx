import React from "react";
import Navbar from "../Navbar";

function index() {
  return (
    <body>
      <Navbar />

      <footer className="footer">
        <p className="footer-by">
          A project by{" "}
          Jocner Patiño Buznego
        </p>
      </footer>
    </body>
  );
}

export default index;
