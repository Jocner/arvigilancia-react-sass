import React, { useState, useEffect, useRef, Fragment } from 'react';
// import Navbar from "../Navbar";
import './newauto.scss';
import axios from 'axios';
import Footer from '../Footer';
// import * as dotenv from 'dotenv';
// dotenv.config();


function NewAuto() {
const PATENTE_REGEX = /^[A-z][A-z0-9-_]{3,23}$/;
const MARCA_REGEX = /^[A-z][A-z0-9-_]{3,23}$/;

// const serviceUrl = process.env.REACT_APP_URL_SERVICE;

//     console.log("SERVICE", serviceUrl)

  // const [post, setPost] = useState(null);

    // const patenteRef = useRef();
    const patenteRef = useRef<HTMLInputElement>(null);
    const marcaRef = useRef<HTMLInputElement>(null);
    const propietarioRef = useRef<HTMLInputElement>(null);
    const telefonoRef = useRef<HTMLInputElement>(null);
    const imageRef = useRef<HTMLInputElement>(null);
    const errRef = useRef<HTMLInputElement>(null);

    const [patente, setPatente] = useState('');
    const [validPatente, setValidPatente] = useState(false);
    const [PatenteFocus, setPatenteFocus] = useState(false);

    const [marca, setMarca] = useState('');
    const [validMarca, setValidMarca] = useState(false);
    const [marcaFocus, setMarcaFocus] = useState(false);

    const [propietario, setProprietario] = useState('');
    const [validPropietario, setValidPropietario] = useState(false);
    const [proprietarioFocus, setProprietarioFocus] = useState(false);

    const [telefono, setTelefono] = useState('');
    const [validTelefono, setValidTelefono] = useState(false);
    const [telefonoFocus, setTelefonoFocus] = useState(false);

    const [image, setImage] = useState('');
    const [validImage, setValidImage] = useState(false);
    const [imageFocus, setImageFocus] = useState(false);


    const [errMsg, setErrMsg] = useState('');
    const [success, setSuccess] = useState(false);

//     useEffect(() => {
//       patenteRef.current.focus();
//   }, [])

//   useEffect(() => {
//       setValidPatente(PATENTE_REGEX.test(patente));
//   }, [patente])

//   useEffect(() => {
//       setValidPwd(PWD_REGEX.test(pwd));
//       setValidMatch(pwd === matchPwd);
//   }, [pwd, matchPwd])

//   useEffect(() => {
//       setErrMsg('');
//   }, [user, pwd, matchPwd])




  const handleSubmit = async (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    // const serviceUrl = process.env.REACT_APP_URL_SERVICE;
    // console.log('sdfdsfsdfsdfsd', serviceUrl);
    // if button enabled with JS hack
    const v1 = PATENTE_REGEX.test(patente);
    const v2 = MARCA_REGEX.test(marca);
    if (!v1 || !v2) {
        setErrMsg("Invalid Entry");
        return;
    }
    try {
        const response = await axios.post('http://localhost:4000/vehiculo/',
            JSON.stringify({ patente, marca, propietario, telefono, image }),
            {
                headers: { 'Content-Type': 'application/json' },
                // withCredentials: true
            }
        );
        console.log(response?.data);
        // console.log(response?.accessToken);
        console.log(JSON.stringify(response))
        setSuccess(true);
        //clear state and controlled inputs
        //need value attrib on inputs for this
        setPatente('');
        setMarca('');
        setProprietario('');
        setTelefono('');
        setImage('');
    } catch (err) {
        if (!err) {
            setErrMsg('No Server Response');
        } else if (err === 409) {
            setErrMsg('Username Taken');
        } else {
            setErrMsg('Registration Failed')
        }
        // errRef.current.focus();
    }
}
  return (
   
     
    
       

      
    //   {success ? (
    //     <section>
    //         <h1>Success!</h1>
    //         <p>
    //             <a href="#">Sign In</a>
    //         </p>
    //     </section>
    // ) : (



    <Fragment>
      {/* <body> */}
                <main>
          <span className={"intro"}>
        <section>
            {/* <p ref= {errRef} className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p> */}
            <h5>Register</h5>
            <form onSubmit={handleSubmit}>
                <label htmlFor="patente">
                    Patente:
                </label>
                <input
                    type="text"
                    id="patente"
                    ref={patenteRef}
                    autoComplete="off"
                    onChange={(e) => setPatente(e.target.value)}
                    value={patente}
                    required
                    aria-invalid={validPatente ? "false" : "true"}
                    aria-describedby="uidnote"
                    onFocus={() => setPatenteFocus(true)}
                    onBlur={() => setPatenteFocus(false)}
                />
              


                <label htmlFor="marca">
                    Marca:
                </label>
                <input
                    type="text"
                    id="marca"
                    ref={marcaRef}
                    onChange={(e) => setMarca(e.target.value)}
                    value={marca}
                    required
                    aria-invalid={validMarca ? "false" : "true"}
                    aria-describedby="marca"
                    onFocus={() => setMarcaFocus(true)}
                    onBlur={() => setMarcaFocus(false)}
                />
      


                <label htmlFor="propietario">
                    Propietario:
                </label>
                <input
                    type="text"
                    id="propietario"
                    ref={propietarioRef}
                    onChange={(e) => setProprietario(e.target.value)}
                    value={propietario}
                    required
                    aria-invalid={validPropietario ? "false" : "true"}
                    aria-describedby="propietario"
                    onFocus={() => setProprietarioFocus(true)}
                    onBlur={() => setProprietarioFocus(false)}
                />
           

                <label htmlFor="telefono">
                    Telefono:
                </label>
                <input
                    type="text"
                    id="telefono"
                    ref={telefonoRef}
                    onChange={(e) => setTelefono(e.target.value)}
                    value={telefono}
                    required
                    aria-invalid={validTelefono ? "false" : "true"}
                    aria-describedby="telefono"
                    onFocus={() => setTelefonoFocus(true)}
                    onBlur={() => setTelefonoFocus(false)}
                />
            
                 

                <label htmlFor="image">
                    Image:
                </label>
                <input
                    type="text"
                    id="image"
                    ref={imageRef}
                    onChange={(e) => setImage(e.target.value)}
                    value={image}
                    required
                    aria-invalid={validImage ? "false" : "true"}
                    aria-describedby="pwdnote"
                    onFocus={() => setImageFocus(true)}
                    onBlur={() => setImageFocus(false)}
                />
                <br></br>
                <button className={"regis"}>save</button>
            </form>
       
                
        </section>
        </span>
        </main>
     
  {/* <Footer/> */}
 
    {/* </body>   */}
 </Fragment> 





  );
}

export default NewAuto;
