import "./App.css";
import { HashRouter, Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import Login from "./components/Login";
import Auto from "./components/Auto";
import New from "./components/New";
import Register from "./components/Register";

function App() {
  return (
    <Routes>
      {/* <Route path="profile" element={<Profile />} /> */}
      {/* <Route path="newAuto" element={<NewAuto />} /> */}
      <Route path="new" element={<New />} />
      <Route path="auto" element={<Auto />} />
      <Route path="login" element={<Login />} />
      <Route path="register" element={<Register />} />
      <Route path="/" element={<Home />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export function WrappedApp() {
  return (
    <HashRouter>
      <App />
    </HashRouter>
  );
}
